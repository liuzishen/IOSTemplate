//
//  Image.swift
//  eval
//
//  Created by hengchengfei on 15/9/25.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit

/// 根据颜色生成图片
public func imageWithColor(color: UIColor, size: CGSize, alpha: CGFloat) -> UIImage {
    UIGraphicsBeginImageContext(size)
    let currentContext = UIGraphicsGetCurrentContext()
    let fillRect = CGRectMake(0, 0, size.width, size.height)
    CGContextSetFillColorWithColor(currentContext, color.CGColor)
    CGContextSetAlpha(currentContext, alpha)
    CGContextFillRect(currentContext, fillRect)
    let retval: UIImage = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    return retval
}