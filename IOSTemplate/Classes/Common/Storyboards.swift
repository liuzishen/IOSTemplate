//
//  Storyboards.swift
//  eval
//
//  Created by hengchengfei on 15/9/25.
//  Copyright © 2015年 chengfeisoft. All rights reserved.
//

import UIKit

/// Storyboard CSFTabBar.sotryboard
func CFSStoryboardTabbar<T>(identifier:String) -> T
{
    return UIStoryboard(name: "CFSTabBar", bundle: nil).instantiateViewControllerWithIdentifier(identifier) as! T;
}
/// Storyboard CFSHot.sotryboard
func CFSStoryboardHome<T>(identifier:String) -> T
{
    return UIStoryboard(name: "CFSHome", bundle: nil).instantiateViewControllerWithIdentifier(identifier) as! T;
}
/// Storyboard CFSRank.sotryboard
func CFSStoryboardRank<T>(identifier:String) -> T
{
    return UIStoryboard(name: "CFSRank", bundle: nil).instantiateViewControllerWithIdentifier(identifier) as! T;
}
/// Storyboard CFSUser.sotryboard
func CFSStoryboardUser<T>(identifier:String) -> T
{
    return UIStoryboard(name: "CFSUser", bundle: nil).instantiateViewControllerWithIdentifier(identifier) as! T;
}